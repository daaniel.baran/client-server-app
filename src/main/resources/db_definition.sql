BEGIN;

    CREATE TABLE users (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      username VARCHAR(200) NOT NULL,
      role VARCHAR default ‚USER’,
      password VARCHAR(40) NOT NULL );

    CREATE TABLE messages (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      receiver_id INTEGER NOT NULL,
      text VARCHAR(255),
      sender_id INTEGER NOT NULL);

END;