package run;

import com.google.gson.Gson;
import jdbc.DbConnection;
import jdbc.MessageRepo;
import jdbc.UserRepo;
import message.TextMessage;
import response.Command;
import response.Help;
import response.Info;
import response.Uptime;
import users.Role;
import users.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Server {
    private static final Gson GSON = new Gson();

    private boolean authenticated = false;
    private User currentUser = new User();

    private UserRepo userRepo;
    private MessageRepo messageRepo;

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public Server() {
        try {
            Connection conn = new DbConnection().getConnection();
            this.userRepo = new UserRepo(conn);
            this.messageRepo = new MessageRepo(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void start(int port) throws IOException {

        serverSocket = new ServerSocket(port);
        long serverUp = System.currentTimeMillis();
        Info info = new Info("1.0.0", parseMillisToDate(serverUp));

        System.out.println("[SERVER] SERVER STARTED ON PORT: " + port);
        clientSocket = serverSocket.accept();
        out = new PrintWriter(clientSocket.getOutputStream(), true);

        System.out.println(clientSocket.isConnected() ? "[SERVER] Client connected" : "");
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        out.println("[SERVER] Hello STRANGER, please [LOGIN] or [SIGNUP]");

        String inputLine;

        listening:
        while ((inputLine = in.readLine()) != null) {
            if (isAuthenticated()) {
                boolean isAdmin = isCurrentUserAdmin();

                switch (inputLine) {
                    case "stop":
                        stop();
                        break listening;

                    case "info":
                        out.println(GSON.toJson(info));
                        break;

                    case "help":
                        out.println(GSON.toJson(getAvailableCommands()));
                        break;

                    case "uptime":
                        out.println(GSON.toJson(getServerUpTime(serverUp)));
                        break;

                    case "inbox":
                        checkInbox(getCurrentUser().getId());
                        break;

                    case "clear":
                        out.println(clearUsersInbox(getCurrentUser().getId()));
                        break;

                    case "send":
                        out.println("SEND TO:");
                        String to = in.readLine();

                        if (userRepo.userExists(to)) {
                            out.println("MESSAGE:");

                            String message = in.readLine();

                            out.println(sendMessage(message, to));

                        } else out.println("USER DOESN'T EXISTS");
                        break;
                    case "logout":
                        logout(getCurrentUser());
                        break;

                    //Admin options
                    case "users":
                        if (isAdmin) {
                            out.println("USERS: " + GSON.toJson(userRepo.getUsers()));
                        } else
                            out.println("SORRY, ONLY ADMIN CAN DO THAT!");
                        break;
                    case "add":
                        if (isAdmin) {
                            out.println("TO ADD NEW USER PROVIDE USERNAME:");
                            String userName = in.readLine();

                            out.println("AND PASSWORD");
                            String password = in.readLine();

                            User newUser = new User(userName, password);
                            out.println(addNewUser(newUser));

                        } else
                            out.println("SORRY, ONLY ADMIN CAN DO THAT!");
                        break;

                    case "remove":
                        if (isAdmin) {
                            out.println("PROVIDE NAME TO REMOVE USER");

                            String userName = in.readLine();

                            if (userRepo.userExists(userName)) {
                                if (userName.equals(getCurrentUser().getName())) {
                                    out.println("DO YOU WANT TO DELETE YOUR ACCOUNT? yes or no");

                                    if (in.readLine().equalsIgnoreCase("yes")) {
                                        removeUserByName(userName);

                                        setAuthenticated(false);
                                        setCurrentUser(new User());

                                        out.println("YOUR ACCOUNT HAS BEEN REMOVED!");

                                    } else
                                        out.println("ACCOUNT NOT REMOVED!");
                                } else {
                                    removeUserByName(userName);
                                    out.println("USER:" + userName + " HAS BEEN REMOVED!");
                                }
                            } else out.println("SORRY, THERE IS NO USER: " + userName + "!");

                        } else
                            out.println("SORRY, ONLY ADMIN CAN DO THAT!");
                        break;

                    case "change-role":
                        if (isAdmin) {
                            out.println("PROVIDE NAME TO CHANGE RIGHTS");
                            User user = getUserByName(in.readLine());

                            out.println(userRepo.changeRole(user.getName(), user.getRole().name()));
                        } else
                            out.println("SORRY, ONLY ADMIN CAN DO THAT!");
                        break;

                    case "details":
                        if (isAdmin) {
                            out.println("PROVIDE USERNAME TO SEE DETAILS:");
                            String name = in.readLine();

                            if (userRepo.userExists(name)) {
                                out.println(GSON.toJson(getUserByName(name)));
                            } else
                                out.println("SORRY, THERE IS NO USER: " + name + "!");
                        } else
                            out.println("SORRY, ONLY ADMIN CAN DO THAT!");
                        break;
                    default:
                        out.println("UNKNOWN COMMAND");
                        break;
                }
            } else {
                if (inputLine.equalsIgnoreCase("login")) {

                    out.println("PROVIDE LOGIN:");
                    String login = in.readLine();
                    out.println("AND PASSWORD:");
                    String password = in.readLine();

                    setCurrentUser(new User(login, password));

                    if (authenticateUser(getCurrentUser()))
                        out.println("LOGGED IN! NOW WHAT? uptime, info, help, stop, inbox, send, clear, users, add, remove, details, change-role, and logout");
                    else
                        out.println("WRONG CREDENTIALS, PLEASE CHECK OR SIGNUP!");

                } else if (inputLine.equalsIgnoreCase("signup")) {
                    User newUser = new User();

                    out.println("PROVIDE LOGIN:");
                    String login = in.readLine();

                    if (!userRepo.userExists(login)) {
                        newUser.setName(login);

                        out.println("NOW PASSWORD:");
                        String password = in.readLine();

                        newUser.setPassword(password);

                        setCurrentUser(newUser);

                        userRepo.addUser(getCurrentUser());
                        out.println("SIGNUP SUCCESSFUL! PLEASE LOGIN NOW!");
                    } else
                        out.println("USERNAME TAKEN, TRY AGAIN OR SIGNUP!");
                } else
                    out.println("SORRY I DON'T KNOW THIS COMMAND");
            }
        }
    }

    private void logout(User user) {
        System.out.println("[SERVER] USER: " + user.getName() + " logged out");

        out.println("YOU'VE BEEN LOGOUT ;) WHAT'S UP?");

        setCurrentUser(new User());
        setAuthenticated(false);
    }

    private void checkInbox(int id) {
        List<TextMessage> messages = messageRepo.getAllMessagesByUserId(id);
        if (messages.isEmpty())
            out.println("INBOX IS EMPTY!");
        else {
            StringBuilder response = new StringBuilder();

            for (TextMessage message : messages) {
                response.append("|").append(userRepo.getUserById(message.getFrom()).getName()).append("| WROTE:'").append(message.getText()).append("' -----");
            }

            response = new StringBuilder(response.substring(0, response.length() - 5));
            out.println(response);
        }
    }

    private String clearUsersInbox(int id) {
        return messageRepo.clearUserInbox(id);
    }

    public boolean isCurrentUserAdmin() {
        return getCurrentUser().getRole().equals(Role.ADMIN);
    }

    public boolean authenticateUser(final User tempUser) {
        boolean exists = userRepo.userExists(tempUser.getName());

        if (exists) {
            final User user = getUserByName(tempUser.getName());
            if (user.getPassword().equals(tempUser.getPassword())) {
                System.out.println("[SERVER] |" + tempUser.getName() + "| logged in!");

                setCurrentUser(user);
                setAuthenticated(true);
                return true;
            }
        }

        System.out.println("[SERVER] WRONG CREDENTIALS!");
        setAuthenticated(false);
        return false;
    }

    public User getUserByName(String name) {
        return userRepo.getUserByName(name);
    }


    public String addNewUser(User newUser) {
        return userRepo.addUser(newUser);
    }

    public void removeUserByName(String userName) {
        userRepo.removeUser(userName);
    }

    public String sendMessage(String text, String sendTo) {
        boolean tooLong = text.length() > 255;
        User userTo = getUserByName(sendTo);
        List<TextMessage> usersMessages = messageRepo.getAllMessagesByUserId(userTo.getId());

        if (tooLong) {
            return "SORRY, message is too long. PLEASE try again!";

        } else if (usersMessages.size() < 5) {
            TextMessage message = new TextMessage(text, getCurrentUser().getId(), userTo.getId());
            messageRepo.saveMessage(message);
            return "MESSAGE, SENT";
        } else
            return "SORRY, USER'S INBOX IS FULL";
    }

    public boolean isAuthenticated() {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    private String parseMillisToDate(long serverUp) {
        DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
        Date result = new Date(serverUp);

        return simple.format(result);
    }

    private Help getAvailableCommands() {
        List<Command> commands = new ArrayList<>();
        commands.add(new Command("info", "Outputs version of the server, and creation date"));
        commands.add(new Command("uptime", "Outputs uptime of the server."));
        commands.add(new Command("help", "Outputs list of available commands with a short description."));
        commands.add(new Command("stop", "Stops server and client."));
        commands.add(new Command("inbox", "Check if there are new messages in inbox."));
        commands.add(new Command("clear", "Remove all messages from inbox"));
        commands.add(new Command("send", "Send message to other user."));

        return new Help(commands);
    }

    private Uptime getServerUpTime(long serverTime) {

        long diff = System.currentTimeMillis() - serverTime;

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long days = diff / daysInMilli;
        diff = diff % daysInMilli;

        long hours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long minutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long seconds = diff / secondsInMilli;

        return new Uptime(days, hours, minutes, seconds);
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        clientSocket.close();
        serverSocket.close();

        out.println("SERVER STOPPED!");
        System.out.println("[SERVER] Server stopped");
    }

    public static void main(String[] args) {
        Server server = new Server();

        try {
            server.start(4444);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UserRepo getUserRepo() {
        return userRepo;
    }

    public MessageRepo getMessageRepo() {
        return messageRepo;
    }
}

