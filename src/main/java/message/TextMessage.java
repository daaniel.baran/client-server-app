package message;

public class TextMessage {

    private String text;
    private int senderId;
    private int receiverId;

    public TextMessage(String text, int from, int to) {
        this.text = text;
        this.senderId = from;
        this.receiverId = to;
    }

    public TextMessage() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getFrom() {
        return senderId;
    }

    public void setFrom(int from) {
        this.senderId = from;
    }

    public int getTo() {
        return receiverId;
    }

    public void setTo(int to) {
        this.receiverId = to;
    }
}
