package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

    private static final String URL = "jdbc:sqlite:src/main/resources/client-server.db";

    private final Connection connection;

    public DbConnection() throws SQLException {
        this.connection = DriverManager.getConnection(URL);
    }

    public Connection getConnection() {
        return connection;
    }
}
