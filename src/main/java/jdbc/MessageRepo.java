package jdbc;

import java.sql.Connection;

import message.TextMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageRepo {

    private final Connection connection;

    public MessageRepo(Connection conn) {
        this.connection = conn;
    }

    public void saveMessage(TextMessage message) {
        final String query = "INSERT INTO messages (receiver_id, text, sender_id) VALUES(?,?,?)";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, message.getTo());
            stmt.setString(2, message.getText());
            stmt.setInt(3, message.getFrom());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<TextMessage> getAllMessagesByUserId(int userId) {
        final String query = "SELECT sender_id, text, id FROM messages WHERE receiver_id=?";

        List<TextMessage> textMessages = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Integer sender = rs.getInt("sender_id");
                String text = rs.getString("text");
                textMessages.add(new TextMessage(text, sender, userId));
            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("[SERVER] NO MESSAGES!");
        }
        return textMessages;
    }

    public String clearUserInbox(int id) {

        final String query = "DELETE FROM messages WHERE receiver_id=?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "USER INBOX IS EMPTY NOW!";
    }
}
