package jdbc;

import users.Role;
import users.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepo {

    private final Connection connection;

    public UserRepo(Connection connection) {
        this.connection = connection;
    }

    public String addUser(User newUser) {
        boolean userExists = userExists(newUser.getName());

        if (userExists) {
            return "USER ALREADY EXISTS";
        } else {
            final String query = "INSERT INTO users (username, password, role ) VALUES(?,?,?)";

            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, newUser.getName());
                stmt.setString(2, newUser.getPassword());
                stmt.setString(3, newUser.getRole().name());
                stmt.executeUpdate();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            notifyServer("NEW USER: |" + newUser.getName() + "| REGISTERED!");
            return "USER CREATED";
        }
    }

    public String removeUser(String username) {
        final String query = "DELETE FROM users WHERE username=?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, username);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        notifyServer("[SERVER] USER REMOVED: " + username);
        return "USER REMOVED";
    }

    public boolean userExists(String username) {
        final String query = "SELECT COUNT(1) FROM users WHERE username = ?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, username);

            ResultSet rs = stmt.executeQuery();
            int size = rs.getInt(1);

            if (size == 1) return true;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String changeRole(String username, String role) {

        String query = "UPDATE users SET role = ? WHERE username = ?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, role.equals("USER") ? "ADMIN" : "USER");
            stmt.setString(2, username);
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "ROLE UPDATED!";
    }

    public List<User> getUsers() {
        final String query = "SELECT id, username, password, role FROM users";
        List<User> users = new ArrayList<>();

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = rs.getString("password");
                String role = rs.getString("role");

                users.add(new User(id, username, password, Role.valueOf(role)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public User getUserByName(String username) {
        final String query = "SELECT id, username, password, role FROM users WHERE username=?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, username);

            ResultSet rs = stmt.executeQuery();

            int id = rs.getInt("id");
            String name = rs.getString("username");
            String password = rs.getString("password");
            String role = rs.getString("role");

            return new User(id, name, password, Role.valueOf(role));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new User();
    }

    private void notifyServer(String message) {
        System.out.println(message);
    }

    public User getUserById(int userId) {
        final String query = "SELECT id, username, password, role FROM users WHERE id=?";

        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setInt(1, userId);

            ResultSet rs = stmt.executeQuery();

            int id = rs.getInt("id");
            String name = rs.getString("username");
            String password = rs.getString("password");
            String role = rs.getString("role");

            return new User(id, name, password, Role.valueOf(role));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new User();
    }
}
