import jdbc.MessageRepo;
import jdbc.UserRepo;
import message.TextMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import run.Server;
import users.Role;
import users.User;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BasicUnitTests {

    Server server;
    User temp = new User("name", "pass");
    UserRepo userRepo;
    MessageRepo messageRepo;

    @BeforeEach
    void setUp() throws SQLException {
        server = new Server();
        server.addNewUser(temp);
        userRepo = server.getUserRepo();
        messageRepo = server.getMessageRepo();
    }

    @Test
    void ensureAuthenticationWorks() {
        User fakeUser = new User("fake", "facepalms");
        User realUser = server.getUserByName("daniel");

        assertFalse(server.authenticateUser(fakeUser));
        assertTrue(server.authenticateUser(realUser));
    }

    @Test
    void ensureInboxCanOnlyStoreUpTo5Messages() {
        User user = server.getUserByName("daniel");
        messageRepo.clearUserInbox(user.getId());

        for (int i = 0; i < 6; i++) {
            server.sendMessage("hello " + i, "daniel");
        }

        int inboxSize = messageRepo.getAllMessagesByUserId(user.getId()).size();

        assertEquals(5, inboxSize, "user has : " + inboxSize + " messages");
        assertNotEquals(6, inboxSize);
    }

    @Test
    void ensureNewUserDoesNotHaveAdminAccess() {
        User newUser = new User();
        assertNotEquals(Role.ADMIN, newUser.getRole());
    }

    @Test
    void ensureMessagesUpToo255CharsAreAcceptable() {
        String messageLongerThan255Chars = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.";
        String response = server.sendMessage(messageLongerThan255Chars, "daniel");

        assertEquals("SORRY, message is too long. PLEASE try again!", response);
    }


    @Test
    void ensureSendingAMessageIsPossible() {
        User user = userRepo.getUserByName("daniel");
        messageRepo.clearUserInbox(user.getId());

        String testMessage = "hey daniel";

        server.sendMessage(testMessage, user.getName());

        List<TextMessage> usersMessages = server.getMessageRepo().getAllMessagesByUserId(user.getId());

        assertTrue(usersMessages.stream().anyMatch(message -> message.getText().equals(testMessage)));
    }

    @Test
    void ensureIsNotPossibleToAddTwiceTheSameUser() {
        User user = new User("user", "password");
        assertEquals(server.addNewUser(user), "USER ALREADY EXISTS");
    }

    @Test
    void ensureYouCanCheckIfUserAlreadyExists() {
        String realName = "daniel";
        String fakeName = "fakeName";

        assertTrue(userRepo.userExists(realName));
        assertFalse(userRepo.userExists(fakeName));
    }

    @Test
    void ensureRemovingRegularUsersIsPossible() {
        assertEquals(userRepo.removeUser(temp.getName()), "USER REMOVED");
    }
}
